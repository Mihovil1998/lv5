﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();
    }
}