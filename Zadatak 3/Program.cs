﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data1 = new Dataset("C:\\Users\\Mihovil\\Desktop\\4. semestar\\RPPOON\\LV5\\Z3.txt");
            User user1 = User.GenerateUser("user1");
            User user2 = User.GenerateUser("user2");
            User user3 = User.GenerateUser("user3");
            User user4 = User.GenerateUser("user4");
            User user5 = User.GenerateUser("user5");
            ProtectionProxyDataset proxy1 = new ProtectionProxyDataset(user1);
            ProtectionProxyDataset proxy2 = new ProtectionProxyDataset(user2);
            ProtectionProxyDataset proxy3 = new ProtectionProxyDataset(user3);
            ProtectionProxyDataset proxy4 = new ProtectionProxyDataset(user4);
            ProtectionProxyDataset proxy5 = new ProtectionProxyDataset(user5);
            VirtualProxyDataset proxy6 = new VirtualProxyDataset("C:\\Users\\Mihovil\\Desktop\\4. semestar\\RPPOON\\LV5\\Z3.txt");
            DataConsolePrinter printer = new DataConsolePrinter();
            printer.Print(proxy1);
            Console.WriteLine();
            printer.Print(proxy2);
            Console.WriteLine();
            printer.Print(proxy3);
            Console.WriteLine();
            printer.Print(proxy4);
            Console.WriteLine();
            printer.Print(proxy5);
            Console.WriteLine();
            Console.WriteLine("Virtual: \n");
            printer.Print(proxy6);
        }
    }
}
