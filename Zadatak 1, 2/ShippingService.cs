﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1__2
{
    class ShippingService
    {
        private double unitPrice;

        public ShippingService(double unitPrice)
        {
            this.unitPrice = unitPrice;
        }

        public double Price(double weight)
        {
            return unitPrice * weight;
        }

        public override string ToString()
        {
            return "Cijena dostave paketa iznosi: ";
        }
    }
}
