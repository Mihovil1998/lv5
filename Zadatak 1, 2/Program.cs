﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak_1__2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IShipable> list = new List<IShipable>();
            Box box = new Box("Kutija");
            Product product1 = new Product("Acer Swift 3", 1, 10000);
            Product product2 = new Product("Fender Jazz Bass", 3, 20000);

            list.Add(box);
            list.Add(product1);
            list.Add(product2);

            ShippingService HP = new ShippingService(5);
            double weight = 0;

            foreach (IShipable ship in list)
            {
                weight += ship.Weight;
                Console.WriteLine(ship.Description());
            }

            Console.WriteLine(HP.ToString() + HP.Price(weight) + " HRK");
        }
    }
}
